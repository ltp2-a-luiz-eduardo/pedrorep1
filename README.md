# Guia de Comandos Git

Guia de trabalho para auxiliar nas possiveis duvidas relacionadas a git.

## Comanados

### git branch [nome-da-branch]

* Permite criar, listar, renomear e excluir ramificações

### git brach -d [nome-da-branch]

* Exclui uma ramificação. A ramificação deve ser totalmente mesclada em sua ramificação upstream ou em HEAD se nenhum upstream foi definido com --track ou --set-upstream-to.

### git checkout [nome-da-branch]

* Para se preparar para trabalhar em branch, mude para ele atualizando o índice e os arquivos na árvore de trabalho e apontando HEAD para a ramificação. As modificações locais nos arquivos da árvore de trabalho são mantidas, para que possam ser enviadas para o branch.

### git branch -a [nome-da-branch]

* Lista filiais de rastreamento remoto e filiais locais.

### git checkout -b [nome-da-branch]

* Crie uma nova ramificação chamada new-branch, e verifica a ramificação resultante.

### git pull

* Incorpora alterações de um repositório remoto na ramificação atual.

### git commit -m

* Cria um novo commit contendo o conteúdo atual do índice e a mensagem de log fornecida descrevendo as alterações.

### git add .

* Este comando atualiza o índice usando o conteúdo atual encontrado na árvore de trabalho, para preparar o conteúdo preparado para o próximo commit.

### git push

* Atualiza referências remotas usando referências locais, enquanto envia objetos necessários para completar as referências fornecidas.

### git init

* Este comando cria um repositório Git vazio - basicamente um diretório .git com subdiretórios para objetos, refs/heads, refs/tags e arquivos de modelo.

* -q
--quiet

    This is passed to both underlying git-fetch to squelch reporting of during transfer, and underlying git-merge to squelch output during merging.

